<?php
namespace uhi67\selectfrom;

class SelectFromAsset extends \yii\web\AssetBundle
{
    // The files are not web directory accessible, therefore we need
    // to specify the sourcePath property. Notice the @bower alias used.
    public $sourcePath = '@vendor/uhi67/selectfrom/assets';

    public $css = [
        'selectfrom.less',
    ];

    public $js = [
        'selectfrom.js',
    ];

    public $depends = [
        'yii\web\JqueryAsset',
        'yii\jui\JuiAsset'
    ];
	public $publishOptions = [
		'forceCopy' => YII_ENV_DEV, 
	];
}
