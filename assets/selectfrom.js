/**
 * selectfrom.js
 * SelectFromWidget
 *
 */
 
console.log('selectfrom.js v1.0');

$.fn.extend({
	selectfromquery: function(url, data, options) {
		var $dialog = $(this);
		var options1 = options; // For the scope
		$.ajax(url, {
			context: $dialog.get(0),
			data: data,
			error: function(jqXHR, textStatus, error) {
				$(this).html('<h1>'+textStatus+'</h1><p>'+error+'<p>');
			},
			success: function(data, textStatus, jqXHR) {
				var dialog = this;
				$(dialog).html(data);
				var target = $(dialog).data('target');
				var $block = $('.select-from-block', this);
				// Updating clicks
				$('.select-from-return', this).click(function() {
					var map = $(target).data('map');
					console.log('return', $(this).data('id'), map, target);
					// TODO: use map
					$(target).val($(this).data('id'));
					$(target).parent().find('.select-from-name').val($(this).data('name'));
					
					$(dialog).dialog('close');
				});
				$('.select-from-reload', this).click(function() {
					var localdata = $(this).data('data');
					var globaldata = $block.data('reload');
					console.log('reload', localdata, globaldata);
					var data = $.extend({}, globaldata, localdata);
					console.log('reload', data, target);
					$(dialog).selectfromquery(url, data, options1);
				});
			},
			timeout: options.hasOwnProperty('timeout') ? options.timeout : 3000,
			
		});
	}
});

$(function() {
	$('.select-from-select, .select-from-overlay').click(function() {
		var $group = $(this).closest('.input-group');
		var id = $group.prop('id');
		var $target = $('.select-from-target', $group);
		var $name = $('.select-from-name', $group);
		var url = $target.data('ajax-url');
		var data = $target.data('ajax-data');
		var options = $target.data('ajax-options');
		var dialog = $target.data('dialog');
		var $dialog = $('#'+dialog);
		$dialog.html('<p>Kérem, várjon...</p>');
		$dialog.dialog('open');
		$dialog.data('target', $target.get(0));
		$dialog.parent().find('button.ui-dialog-titlebar-close').html('<span class="glyphicon glyphicon-remove"></span>');
		$dialog.selectfromquery(url, data, options);
	});
  		
	$('.select-from-clear').click(function(){
		var $group = $(this).closest('.input-group'); 
		var $target = $('.select-from-target', $group);
		var $name = $('.select-from-name', $group);
		var dialog = $target.data('dialog');
		var $dialog = $('#'+dialog);
		$dialog.dialog('close');
		$target.val('');
		$name.val('');
	});
	
});

function escapeHtml(text) {
  var map = {
    '&': '&amp;',
    '<': '&lt;',
    '>': '&gt;',
    '"': '&quot;',
    "'": '&#039;'
  };
  return text.replace(/[&<>"']/g, function(m) { return map[m]; });
}
