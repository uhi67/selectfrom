selectfrom widget
=================

A widget component for Yii Framework 2.0 to create input fields to select values into from ajax sources via dialog window.

## Installation

The preferred way to install this extension is through [composer](http://getcomposer.org/download/).
To install, either run

	$ php composer.phar require uhi67/selectfrom "1.0.*"

or add

	"uhi67/selectfrom" : "1.0.*"

or clone form github

	git clone https://bitbucket.org/uhi/selectfrom.git

### Pre-requisites
  
## Usage

### Basic Usage in activeForm

In the view

	<?php $form = ActiveForm::begin(); ?>
		<?= $form->field($model, 'parent', ['labelOptions'=>['class'=>'col-md-4']])->widget(SelectFromWidget::className(), [
			'title' => 'Choose parent department',
			'nameValue' => $model->parent0->name,
			'ajax' => Url::to(['/org/selectfrom/']),
			'data' => ['s'=>$model->parent, 'd'=>$model->id, 'c'=>$model->parent],
		]) ?>
	<?php ActiveForm::end(); ?>

Created html example

	<div id="w1" class="input-group select-from-group col-md-8">
		<input type="hidden" id="org-parent_id" 
			class="form-control select-from-target" 
			name="Org[parent]" value="#current_value#" 
			data-dialog="w0" data-maxoccurs="1" data-minoccurs="0" 
			data-ajax-url="/org/selectfrom" 
			data-ajax-data="{&quot;s&quot;:336,&quot;d&quot;:345,&quot;c&quot;:336}" 
			data-map="[]"
	  	/>
		<div class="select-from-wrapper">
			<input type="text" id="org-parent_name" name="Org[parent]_name" value="#name_of_the_current_value#" disabled="disabled"
				class="form-control select-from-name" 
			/>
			<div class="select-from-overlay"/>
		</div>
		<span class="input-group-btn select-from-select">
			<button type="button" class="btn btn_primary">
				<span class="glyphicon glyphicon-triangle-bottom"></span>
			</button>
		</span>
		<span class="input-group-btn select-from-clear">
			<button type="button" class="btn">
				<span class="glyphicon glyphicon-remove"></span>
			</button>
		</span>
	</div>

Ajax-provided content must return two or optionally more data using class="select-from-return" data-id="id" data-name="name" data-other="other values for optional mapped fields"
Ajax content may be recall itself using class="select-from-reload" and data-* fields merged with optional global data from select-from-block (see example)
Ajax content may use select-from-disabled and select-from-current for coloring purposes (will not return values)
The returned id and name will be copied into hidden anad name fields, the optional values will be copied into external fields specified by field map. 

Demanded ajax content example:

	<div class="select-from-block" data-reload="{'d':1, 'c':2}">
		<ul>
			<li class="select-from-reload" data-id="3">A reload-item</i>
			<li class="select-from-return" data-id="4" data-name="Some-visible-name" data-field3="It's an extra data">Some-visible-name</li>
			<li class="select-from-disabled">A non-selectable item</li>
			<li class="select-from-current">Current value (not selectable)</li>
		</ul>
	</div>

### Usage example in kartik\detail\DetailView

	[
		'attribute' => 'org', 
		'value' => ($org_name = ArrayHelper::getValue($model->org0, 'name')),
		'type' => DetailView::INPUT_WIDGET,
		'widgetOptions' => [
			'class' => SelectFromWidget::className(),
	    	'title' => 'Please select an owner department...',
	    	'options' => ['class'=>'input-sm'], // html options for all input elements
	    	'nameOptions' => ['placeholder'=>'Select...', 'class'=>'input-sm'], // html options for name input
	    	'nameValue' => $org_name,
	    	'ajax' => Url::to(['/org/selectfrom/']),
	    	'data' => ['s'=>$model->parent, 'c'=>$model->parent],
	    	'ajaxOptions' => ['timeout' => 6000],
		], 
	],

## License

Copyright (c) 2017, Uherkovich Péter  
uhisoft.hu  
All rights reserved. 

"uhi67/selectfrom" is licensed under GNU General Public License v3

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
See the bundled `LICENSE` for details.