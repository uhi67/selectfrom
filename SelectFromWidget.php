<?php
/**
 * @package   uhi67/selectfrom
 * @link https://bitbucket.org/uhi/selectfrom
 * @author Uherkovich Péter <uhi@uhisoft.hu>
 * @copyright 2017 Uherkovich Péter GNU-GPLv3
 * @version 1.0
 */
namespace uhi67\selectfrom;

use yii;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\jui\Dialog;

/**
 * SelectFromWidget
 * ================
 * Creates a hidden input for value, a visible input for displayable name, and a modal window with ajax content.
 * Optionally creates select and/or clear buttons.
 *  
 * @link https://bitbucket.org/uhi/selectfrom
 * @author Uherkovich Péter <uhi@uhisoft.hu>
 * @copyright 2017 Uherkovich Péter GNU-GPLv3
 */
class SelectFromWidget extends \yii\widgets\InputWidget {
	/**
     * @var array $options the HTML attributes for the input tag.
     * @see \yii\helpers\Html::renderTagAttributes() for details on how attributes are being rendered.
	 * Class property will be applied to all visible inputs, including buttons.
	 * Other options will be applied to all inputs, excluding buttons.
	 * For name-input-specific options, see separate nameOptions.
	 */
	 
	/**
	 * Title of dialog box
	 * @var string $title
	 */ 
	public $title;
	/**
	 * Displayable initial value
	 * @var string $nameValue
	 */ 
	public $nameValue;
    /**
     * url to provide ajax data into dialog
     * @see \yii\helpers\BaseUrl::to()
     * @var string $ajax
     * mandatory parameter
     */
    public $ajax;
    /**
     * data for ajax calls
     * @see \yii\helpers\BaseUrl::to()
     * @var array $data
     * mandatory parameter
     */
    public $data;
    /**
     * Multiple selection allowed (later)
     * @var integer $maxoccurs
     */
    public $maxoccurs = 1;
    /**
     * Mandatory (later)
     * @var integer $minoccurs
     */
    public $minoccurs = 0;
    /**
     * Dialog box options, see JQueryUI documentation
     * Defaults (you may override them)
     * 	'classes' => ['ui-dialog'=>'select-from-dialog'],
     * 	'title' => $this->title,
     * 	'autoOpen' => false,
     * 	'height' => 400,
     * 	'width' => 400,
     * 	'modal' => true,
     * @var array $clientOptions
     */
    public $clientOptions = [];
    /**
     * contains index=>fieldid pairs, where index is the index of returned extra data by widget, and fieldid is the target field's id on the form.
     * Non-mapped extra data will be ignored. Non-returned but mapped extra data's fields will be untouched. Returned null's will not affect their fields.
     * 'id' and 'name' will be mapped automatically, unless other values provided.
     * @var array $fieldMap
     */
    public $fieldMap = [];
    /**
     * name field's HTML options. Will be merged with 'options'
     * @var array $nameOptions
     */
    public $nameOptions = [];
    /**
     * Options for ajax calls.
     * Currently the only option is 'timeout' which has a default of 3000 ms
     */
    public $ajaxOptions = [];

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
   	}
   	
    /**
     * @inheritdoc
     */
    public function run()
    {
    	$items = ['0'=>'...Select...'];
        $name_name = ArrayHelper::getValue($this->options, 'name', Html::getInputName($this->model, $this->attribute)).'_name';
        $id = ArrayHelper::getValue($this->options, 'id', Html::getInputId($this->model, $this->attribute));
        
		// Adds dialog
		#$content .= Html::tag('div', '<p>Please wait...</p>', ['class'=>'select-from-dialog ui-widget', 'id'=>'select-from-dialog-'.$this->id]);
		$dialog = Dialog::begin([
			'clientOptions' => array_merge([
				'classes' => ['ui-dialog'=>'select-from-dialog'],
				'title' => $this->title,
				'autoOpen' => false,
				'height' => 400,
				'width' => 400,
				'modal' => true,
			], $this->clientOptions),
		]);
		echo 'Please wait...'; 
		Dialog::end();
        $this->options['data-dialog'] = $dialog->id;

		// Creates hidden target. This value will be posted with the hosting form
		$hiddenoptions = array_merge($this->options, [
			'id' => $id.'_id',
	        'data-maxoccurs' => $this->maxoccurs,
	        'data-minoccurs' => $this->minoccurs,
	        'data-ajax-url' => Url::to($this->ajax),
	        'data-ajax-data' => json_encode($this->data),
	        'data-ajax-options' => json_encode($this->ajaxOptions),
	        'data-map' => json_encode($this->fieldMap),
		]);
		self::addName($hiddenoptions['class'], ['form-control', 'select-from-target']);
		if ($this->hasModel()) {
            $content = Html::activeHiddenInput($this->model, $this->attribute, $hiddenoptions);
        } else {
            $content = Html::hiddenInput($this->name, $this->value, $hiddenoptions);
        }
		
		// Creates disabled input for name with clickable overlay. This value will not be posted.
		$nameOptions = array_merge($this->options, array_merge($this->nameOptions, [
					'id' => $id.'_name',
					'disabled' => 'disabled',
				]));
		self::addName($nameOptions['class'], ['form-control', 'select-from-name']);
		$content .=
			Html::tag('div',  
				Html::textInput($name_name, $this->nameValue, $nameOptions). Html::tag('div', '', ['class'=>'select-from-overlay']),
				['class'=>'select-from-wrapper']
			);
		$buttonclass = $this->options['class'];
		self::addName($buttonclass, ['btn', 'btn_primary']);
        $content .= Html::tag('span', 
			Html::tag('button', 
				html::tag('span', '', ['class'=>'glyphicon glyphicon-triangle-bottom']), 
				['class'=>$buttonclass, 'type'=>'button']
			), 
			['class'=>'input-group-btn select-from-select']
		);
		
		// Adds clear button if value is not mandatory
		$buttonclass = $this->options['class'];
		self::addName($buttonclass, ['btn']);
		if($this->minoccurs<1){
	        $content .= Html::tag('span', 
				Html::tag('button', 
					html::tag('span', '', ['class'=>'glyphicon glyphicon-remove']),
					['class'=>$buttonclass, 'type'=>'button']
				), 
				['class'=>'input-group-btn select-from-clear']
			);
		}
		
    	echo Html::tag('div', $content, ['class'=>'input-group select-from-group col-md-8', 'id'=>$this->id]);

        $this->registerAssets();
    }
    
    /**
     * Adds a name or list of names to the list if not exists
     * The list is a space-separated name list, like classnames in html.
     * Modifies the input array itself!
     * 
     * @param array $list
     * @param string|array $names -- names to add
     * @return array
     */
    static function addName(&$list, $names) {
    	if(!is_array($names)) $names = [$names];
    	foreach($names as $name) if(!preg_match("/\w$name\w/", $list)) $list .= ' '.$name;
    	return $list; 
    }
    
    /**
     * Registers Assets
     */
    public function registerAssets()
    {
        $view = $this->getView();
        $bandle = SelectFromAsset::register($view);
	}
}
